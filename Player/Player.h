#ifndef PLAYER_H
#define PLAYER_H

#include "Arduino.h"


class player{
public:
  player(int _tag,int _color); //Constructor
  void resumen(int puntos1,int puntos2,int lvls1,int lvls2,int enemies1,int enemies2); //Pantalla resumen de partida multijugador
  void mjnext_lvl(); //Inicio del turno multijugador
  void mjdead(); //Pantalla de muerte multijugador
  void dead(); //Lo que muestra en pantalla cuando el jugador muere
  void impacto(); //En caso de que el disparo impacte
  void next_lvl(); //Pantalla siguiente nivel
  void desplazar(); //Desplazamiento del jugador en pantalla
  void disparar(); //Disparo de la nave
  void init(); //Inicializa la pantalla
  int menu(); //Menu de inicio del juego
  void GUI(); //Interfaz de usuario, puntuacion y viads
  void colide(); //En caso de que al jugador lo goolpee algun proyectil
public:
  int disx; //Posicion del disparo en X
  int disy = posy - 19; //Posicion del disparo en Y (Desplazada 19 pixeles hacia arriba para que no sobreescriba la nave)
  int disdraw = false; //Dibujar disparo
  int posx = 0;   //Posicion en X de la nave
  int posy = 102; // Posicion en Y de la nave
  int ancho = 23; //Ancho de la nave
  int largo = 26; //Largo de la nave
  int vidas = 3; //Número de vidas del jugador
  bool draw = true; //Dibujar la nave
  int puntos = 0; //Puntaje del jugador
  int nivel = 0; //Nivel del jugador
  int tag; //Etiqueta de jugador
  int color; //Color de la nave
  int enemies; //Enemigos derrotados
};

class alienT1{
public:
    alienT1(); //Constructor
    void desplazar(); //Desplaza y dibuja el alien
    void impacto(); //Lo que hace el alien en caso de impacto
    void explotar(); //Animacion de explosion
public:
    unsigned long tdesp; //Tiempo de desplazamiento
    unsigned long tsprite; //Tiempo de dibujado de los sprites
    int velocidad; //Velocidad de desplazamiento del alien
    int posx; //Posicion en X del alien
    int posy; //Posicion en Y del alien
    int posix; //Posicion inicial en X del alien
    int ancho; //Ancho de la imagen del alien
    int largo; //Largo de la imagen del alien
    bool draw; //Dibujar alien
    bool impact; //Impacto sobre el alien
};

class alien_h : public alienT1{
public:
    alien_h();
    void atacar(); //Funcion que hace disparar al alien
    void desplazar(); //Desplaza al alien
    void impacto();
    void removeshot();
public:
    int velproyectil;
    int disx;     //Posicion del disparo en X
    int disy;     //Posicion del disparo del alien en Y
    bool disdraw; //Detecta si el alien disparo
    bool shot; //Disparar
};

class boss_a :public alien_h{ //Final boss A
public:
    boss_a();
    void atacar(); //Ataque del alien
    void desplazar(); //Desplaza al alien en pantalla
    void impacto(); //Lo que hace el alien cuando lo impacta un proyectil
    void removeshot(); //Remover el disparo de la pantalla
    void healthbar(); //Barra de vida
public:
    int vida;
};

#endif // PLAYER_H
