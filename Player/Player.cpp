#include "Arduino.h"
#include "Player.h"
#include <Adafruit_GFX.h>
#include <Adafruit_ST7735.h>

#define TFT_CS 5
#define TFT_RST 22 //Puede ser -1 si el reset va al rst del arduino
#define TFT_DC 21

Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_RST); //Conexiones I2C de la pantalla

#define BLACK    0x0000
#define BLUE     0x001F
#define RED      0xF800
#define GREEN    0x07E0
#define CYAN     0x07FF
#define MAGENTA  0xF81F
#define YELLOW   0xFFE0
#define WHITE    0xFFFF

//Array de bits que representan la nave
const unsigned char nave [] PROGMEM = {
  // 'test3, 23x26px
  0x00, 0x38, 0x00, 0x00, 0x38, 0x00, 0x01, 0xff, 0x00, 0x01, 0xff, 0x00, 0x01, 0xff, 0x00, 0x01,
  0xc7, 0x00, 0x01, 0xc7, 0x00, 0x01, 0xc7, 0x00, 0x01, 0xc7, 0x00, 0x01, 0xff, 0x00, 0x01, 0xff,
  0x00, 0x61, 0xff, 0x0c, 0x61, 0xff, 0x0c, 0x61, 0xff, 0x0c, 0xff, 0xff, 0xfe, 0xff, 0xff, 0xfe,
  0xff, 0xff, 0xfe, 0xff, 0xff, 0xfe, 0xe1, 0xff, 0x0e, 0xe1, 0xff, 0x0e, 0xe1, 0xff, 0x0e, 0xe1,
  0xff, 0x0e, 0xe1, 0xff, 0x0e, 0x00, 0x38, 0x00, 0x00, 0x38, 0x00, 0x00, 0x38, 0x00
};

//Array de bits que representan el fondo de la nave
const unsigned char navei [] PROGMEM = {
  // 'test3, 23x26px
  0xff, 0xc7, 0xfe, 0xff, 0xc7, 0xfe, 0xfe, 0x00, 0xfe, 0xfe, 0x00, 0xfe, 0xfe, 0x00, 0xfe, 0xfe,
  0x38, 0xfe, 0xfe, 0x38, 0xfe, 0xfe, 0x38, 0xfe, 0xfe, 0x38, 0xfe, 0xfe, 0x00, 0xfe, 0xfe, 0x00,
  0xfe, 0x9e, 0x00, 0xf2, 0x9e, 0x00, 0xf2, 0x9e, 0x00, 0xf2, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1e, 0x00, 0xf0, 0x1e, 0x00, 0xf0, 0x1e, 0x00, 0xf0, 0x1e,
  0x00, 0xf0, 0x1e, 0x00, 0xf0, 0xff, 0xc7, 0xfe, 0xff, 0xc7, 0xfe, 0xff, 0xc7, 0xfe
};

//Array de bits que representan la bala
const unsigned char bala [] PROGMEM = {
  // 'bala', 3x6px
  0xe0, 0xe0, 0xe0, 0xe0, 0xe0, 0xe0
};

//Array de bits que representan el sprite 0 del alien tipo 1
const unsigned char alien10 [] PROGMEM = {
  //'alien10', 22x16px
  0x0c, 0x00, 0xc0, 0x0c, 0x00, 0xc0, 0x03, 0x03, 0x00, 0x03, 0x03, 0x00, 0x0f, 0xff, 0xc0, 0x0f,
  0xff, 0xc0, 0x3c, 0xfc, 0xf0, 0x3c, 0xfc, 0xf0, 0xff, 0xff, 0xfc, 0xff, 0xff, 0xfc, 0xcf, 0xff,
  0xcc, 0xcf, 0xff, 0xcc, 0xcc, 0x00, 0xcc, 0xcc, 0x00, 0xcc, 0x03, 0xcf, 0x00, 0x03, 0xcf, 0x00
};

//Array de bits que representan el fondo del sprite 0 del alien tipo 1
const unsigned char alien10inv [] PROGMEM = {
  // 'alien1-0, 22x16px
  0xf3, 0xff, 0x3c, 0xf3, 0xff, 0x3c, 0xfc, 0xfc, 0xfc, 0xfc, 0xfc, 0xfc, 0xf0, 0x00, 0x3c, 0xf0,
  0x00, 0x3c, 0xc3, 0x03, 0x0c, 0xc3, 0x03, 0x0c, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x00,
  0x30, 0x30, 0x00, 0x30, 0x33, 0xff, 0x30, 0x33, 0xff, 0x30, 0xfc, 0x30, 0xfc, 0xfc, 0x30, 0xfc
};

//Array de bits que representan el sprite 1 del alien tipo 1
const unsigned char alien11 [] PROGMEM = {
  // 'alien1-1, 22x16px
  0x0c, 0x00, 0xc0, 0x0c, 0x00, 0xc0, 0xc3, 0x03, 0x0c, 0xc3, 0x03, 0x0c, 0xcf, 0xff, 0xcc, 0xcf,
  0xff, 0xcc, 0xfc, 0xfc, 0xfc, 0xfc, 0xfc, 0xfc, 0x3f, 0xff, 0xf0, 0x3f, 0xff, 0xf0, 0x0f, 0xff,
  0xc0, 0x0f, 0xff, 0xc0, 0x0c, 0x00, 0xc0, 0x0c, 0x00, 0xc0, 0x30, 0x00, 0x30, 0x30, 0x00, 0x30
};

//Array de bits que representan el fondo del sprite 1 del alien tipo 1
const unsigned char alien11inv [] PROGMEM = {
  // 'alien1-1, 22x16px
  0xf3, 0xff, 0x3c, 0xf3, 0xff, 0x3c, 0x3c, 0xfc, 0xf0, 0x3c, 0xfc, 0xf0, 0x30, 0x00, 0x30, 0x30,
  0x00, 0x30, 0x03, 0x03, 0x00, 0x03, 0x03, 0x00, 0xc0, 0x00, 0x0c, 0xc0, 0x00, 0x0c, 0xf0, 0x00,
  0x3c, 0xf0, 0x00, 0x3c, 0xf3, 0xff, 0x3c, 0xf3, 0xff, 0x3c, 0xcf, 0xff, 0xcc, 0xcf, 0xff, 0xcc
};

//Array de bits que representan la explosion del alien
const unsigned char boom [] PROGMEM = {
  // 'boom', 22x16px
  0x60, 0x30, 0x60, 0x78, 0x30, 0x60, 0x18, 0x31, 0x80, 0x0c, 0x31, 0x80, 0x0f, 0x36, 0x00, 0x03,
  0x06, 0x00, 0x00, 0x00, 0x00, 0x3e, 0x03, 0xf0, 0x3e, 0x03, 0xf0, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x03, 0x36, 0x00, 0x03, 0x36, 0x00, 0x1c, 0x31, 0x80, 0x7c, 0x31, 0xe0, 0x60, 0x30, 0x60
};

//Array de bits que representan los corazones que inidican el número de vidas
const unsigned char hearth [] PROGMEM = {
        // 'lifes, 11x8px
        0x71, 0xc0, 0xfb, 0xe0, 0xff, 0xe0, 0x7f, 0xc0, 0x3f, 0x80, 0x1f, 0x00, 0x0e, 0x00, 0x04, 0x00
};

//Array de bits que representan el sprite 0 del alien tipo hostil
const unsigned char alien20 [] PROGMEM = {
        // 'alien2-0, 16x16px
        0x03, 0xc0, 0x03, 0xc0, 0x0f, 0xf0, 0x0f, 0xf0, 0x3f, 0xfc, 0x3f, 0xfc, 0xf3, 0xcf, 0xf3, 0xcf,
        0xff, 0xff, 0xff, 0xff, 0x0c, 0x30, 0x0c, 0x30, 0x33, 0xcc, 0x33, 0xcc, 0xcc, 0x33, 0xcc, 0x33
};

//Array de bits que representan el fondo del sprite 0 del alien tipo hostil
const unsigned char alien20inv [] PROGMEM = {
        // 'alien2-0, 16x16px
        0xfc, 0x3f, 0xfc, 0x3f, 0xf0, 0x0f, 0xf0, 0x0f, 0xc0, 0x03, 0xc0, 0x03, 0x0c, 0x30, 0x0c, 0x30,
        0x00, 0x00, 0x00, 0x00, 0xf3, 0xcf, 0xf3, 0xcf, 0xcc, 0x33, 0xcc, 0x33, 0x33, 0xcc, 0x33, 0xcc
};

//Array de bits que representan el sprite 1 del alien tipo hostil
const unsigned char alien21 [] PROGMEM = {
        // 'alien2-1, 16x16px
        0x03, 0xc0, 0x03, 0xc0, 0x0f, 0xf0, 0x0f, 0xf0, 0x3f, 0xfc, 0x3f, 0xfc, 0xf3, 0xcf, 0xf3, 0xcf,
        0xff, 0xff, 0xff, 0xff, 0x00, 0x00, 0x30, 0x0c, 0x18, 0x18, 0x0c, 0x30, 0x04, 0x20, 0x00, 0x00
};

//Array de bits que representan el fondo del sprite 1 del alien tipo hostil
const unsigned char alien21inv [] PROGMEM = {
        // 'alien2-1, 16x16px
        0xfc, 0x3f, 0xfc, 0x3f, 0xf0, 0x0f, 0xf0, 0x0f, 0xc0, 0x03, 0xc0, 0x03, 0x0c, 0x30, 0x0c, 0x30,
        0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xcf, 0xf3, 0xe7, 0xe7, 0xf3, 0xcf, 0xfb, 0xdf, 0xff, 0xff
};

//Array de bits que representan la calavera de la pantalla de muerte
const unsigned char skull [] PROGMEM = {
        // 'skull, 22x18px
        0x30, 0x00, 0x30, 0x30, 0x00, 0x30, 0xf0, 0x00, 0x3c, 0xf0, 0x00, 0x3c, 0x03, 0xff, 0x00, 0x03,
        0xff, 0x00, 0x0f, 0xff, 0xc0, 0x0f, 0xff, 0xc0, 0x0c, 0x30, 0xc0, 0x0c, 0x30, 0xc0, 0x0f, 0xff,
        0xc0, 0x0f, 0xff, 0xc0, 0x03, 0xff, 0x00, 0x03, 0xff, 0x00, 0xf3, 0x33, 0x3c, 0xf3, 0x33, 0x3c,
        0x30, 0x00, 0x30, 0x30, 0x00, 0x30
};

//Array de bits que representa el triangulo de seleccion de los menus
const unsigned char select [] PROGMEM = {
        // 'select, 5x7px
        0xc0, 0xe0, 0xf0, 0xf8, 0xf0, 0xe0, 0xc0
};

const unsigned char bossa0 [] PROGMEM = {
        // 'bossa0, 30x33px
        0x00, 0x7f, 0xf8, 0x00, 0x00, 0x7f, 0xf8, 0x00, 0x00, 0x7f, 0xf8, 0x00, 0x03, 0xff, 0xff, 0x00,
        0x03, 0xff, 0xff, 0x00, 0x03, 0xff, 0xff, 0x00, 0xff, 0x8f, 0xc7, 0xfc, 0xff, 0x8f, 0xc7, 0xfc,
        0xff, 0x8f, 0xc7, 0xfc, 0xff, 0xff, 0xff, 0xfc, 0xe3, 0xff, 0xff, 0x1c, 0xe3, 0xff, 0xff, 0x1c,
        0x03, 0xff, 0xff, 0x00, 0x03, 0xff, 0xff, 0x00, 0x03, 0xff, 0xff, 0x00, 0xff, 0xff, 0xff, 0xfc,
        0xff, 0xff, 0xff, 0xfc, 0xff, 0xff, 0xff, 0xfc, 0xe3, 0xff, 0xff, 0x1c, 0xe3, 0xff, 0xff, 0x1c,
        0xe3, 0xff, 0xff, 0x1c, 0x03, 0x81, 0xc7, 0x00, 0x03, 0x81, 0xc7, 0x00, 0x03, 0x81, 0xc7, 0x00,
        0x03, 0x8e, 0x07, 0x00, 0x03, 0x8e, 0x07, 0x00, 0x03, 0x8e, 0x07, 0x00, 0x1c, 0x0e, 0x00, 0xe0,
        0x1c, 0x0e, 0x00, 0xe0, 0x1c, 0x0e, 0x00, 0xe0, 0x1c, 0x0e, 0x00, 0xe0, 0x1c, 0x0e, 0x00, 0xe0,
        0x1c, 0x0e, 0x00, 0xe0
};

const unsigned char bossa0inv [] PROGMEM = {
        // 'bossa0, 30x33px
        0xff, 0x80, 0x07, 0xfc, 0xff, 0x80, 0x07, 0xfc, 0xff, 0x80, 0x07, 0xfc, 0xfc, 0x00, 0x00, 0xfc,
        0xfc, 0x00, 0x00, 0xfc, 0xfc, 0x00, 0x00, 0xfc, 0x00, 0x70, 0x38, 0x00, 0x00, 0x70, 0x38, 0x00,
        0x00, 0x70, 0x38, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1c, 0x00, 0x00, 0xe0, 0x1c, 0x00, 0x00, 0xe0,
        0xfc, 0x00, 0x00, 0xfc, 0xfc, 0x00, 0x00, 0xfc, 0xfc, 0x00, 0x00, 0xfc, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1c, 0x00, 0x00, 0xe0, 0x1c, 0x00, 0x00, 0xe0,
        0x1c, 0x00, 0x00, 0xe0, 0xfc, 0x7e, 0x38, 0xfc, 0xfc, 0x7e, 0x38, 0xfc, 0xfc, 0x7e, 0x38, 0xfc,
        0xfc, 0x71, 0xf8, 0xfc, 0xfc, 0x71, 0xf8, 0xfc, 0xfc, 0x71, 0xf8, 0xfc, 0xe3, 0xf1, 0xff, 0x1c,
        0xe3, 0xf1, 0xff, 0x1c, 0xe3, 0xf1, 0xff, 0x1c, 0xe3, 0xf1, 0xff, 0x1c, 0xe3, 0xf1, 0xff, 0x1c,
        0xe3, 0xf1, 0xff, 0x1c
};

const unsigned char bossa1 [] PROGMEM = {
        // 'bossa1, 30x33px
        0x00, 0x7f, 0xf8, 0x00, 0x00, 0x7f, 0xf8, 0x00, 0x00, 0x7f, 0xf8, 0x00, 0xe3, 0xff, 0xff, 0x1c,
        0xe3, 0xff, 0xff, 0x1c, 0xe3, 0xff, 0xff, 0x1c, 0xff, 0x8f, 0xc7, 0xfc, 0xff, 0x8f, 0xc7, 0xfc,
        0xff, 0x8f, 0xc7, 0xfc, 0xff, 0xff, 0xff, 0xfc, 0x03, 0xff, 0xff, 0x00, 0x03, 0xff, 0xff, 0x00,
        0xe3, 0xff, 0xff, 0x1c, 0xe3, 0xff, 0xff, 0x1c, 0xe3, 0xff, 0xff, 0x1c, 0xff, 0xff, 0xff, 0xfc,
        0xff, 0xff, 0xff, 0xfc, 0xff, 0xff, 0xff, 0xfc, 0x03, 0xff, 0xff, 0x00, 0x03, 0xff, 0xff, 0x00,
        0x03, 0xff, 0xff, 0x00, 0x03, 0x81, 0xc7, 0x00, 0x03, 0x81, 0xc7, 0x00, 0x03, 0x81, 0xc7, 0x00,
        0x03, 0x81, 0xc7, 0x00, 0x03, 0x81, 0xc7, 0x00, 0x03, 0x81, 0xc7, 0x00, 0x03, 0x81, 0xc7, 0x00,
        0x03, 0x81, 0xc7, 0x00, 0x03, 0x81, 0xc7, 0x00, 0x03, 0x81, 0xc7, 0x00, 0x03, 0x80, 0x07, 0x00,
        0x03, 0x80, 0x07, 0x00
};

const unsigned char bossa1inv [] PROGMEM = {
        // 'bossa1, 30x33px
        0xff, 0x80, 0x07, 0xfc, 0xff, 0x80, 0x07, 0xfc, 0xff, 0x80, 0x07, 0xfc, 0x1c, 0x00, 0x00, 0xe0,
        0x1c, 0x00, 0x00, 0xe0, 0x1c, 0x00, 0x00, 0xe0, 0x00, 0x70, 0x38, 0x00, 0x00, 0x70, 0x38, 0x00,
        0x00, 0x70, 0x38, 0x00, 0x00, 0x00, 0x00, 0x00, 0xfc, 0x00, 0x00, 0xfc, 0xfc, 0x00, 0x00, 0xfc,
        0x1c, 0x00, 0x00, 0xe0, 0x1c, 0x00, 0x00, 0xe0, 0x1c, 0x00, 0x00, 0xe0, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xfc, 0x00, 0x00, 0xfc, 0xfc, 0x00, 0x00, 0xfc,
        0xfc, 0x00, 0x00, 0xfc, 0xfc, 0x7e, 0x38, 0xfc, 0xfc, 0x7e, 0x38, 0xfc, 0xfc, 0x7e, 0x38, 0xfc,
        0xfc, 0x7e, 0x38, 0xfc, 0xfc, 0x7e, 0x38, 0xfc, 0xfc, 0x7e, 0x38, 0xfc, 0xfc, 0x7e, 0x38, 0xfc,
        0xfc, 0x7e, 0x38, 0xfc, 0xfc, 0x7e, 0x38, 0xfc, 0xfc, 0x7e, 0x38, 0xfc, 0xfc, 0x7f, 0xf8, 0xfc,
        0xfc, 0x7f, 0xf8, 0xfc
};
player::player(int _tag,int _color){
    tag = _tag;
    color = _color;
}

void player::impacto(){ //Realiza en caso de que el proyectil impacte
    disdraw = false; // No dibuja mas el disparo
    tft.fillRect(disx,disy,3,128,BLACK); //Quita el proyectil donde impactó
    disy = posy -19; //Reinicia el proyectil en su posicion vertical inicial
}

void player::desplazar(){ //Desplazamiento del jugador
    posx += map(analogRead(A4),0,4095,-1,1+1); //Desplaza la coordenada X de la figura, con el mapeo del joystick
    if(posx >= 137){ // Condicion de extremo derecho de la pantalla
      posx = 137;
   }
  else if (posx <= 0){ // Condicion de extremo izquierdo de la pantalla
    posx = 0;
    }
  tft.drawBitmap(posx, posy, nave, ancho, largo,color); //Dibuja la figura en la posicion desplazada
  tft.drawBitmap(posx,posy,navei,ancho,largo,BLACK); //Dibuja la imagen con color invertido
  tft.fillRect(posx + ancho,posy,ancho,largo,BLACK); //Dibuja los cuadrados que sobreescriben las imagenes
  tft.fillRect(posx - ancho,posy,ancho,largo,BLACK);
}

void player::disparar(){
    if(digitalRead(4) == 0 && disdraw == false){  //Si apreto el boton y no estoy dibujando ningun disparo
      disx = posx + 10; //Posicion en X donde se apreto el boton, mueve la posicion en X al centro del sprite de la nave
      disdraw = true; //Dibujar disparo -> true
      }
    if(disdraw == true){  //Si estoy dibujando disparo
      if(disy > 4){ //Limites de la pantalla
        tft.drawBitmap(disx, disy, bala,3,6,WHITE); //Dibuja el disparo desde la posicion X donde se apreto el boton  y la posicion Y
        tft.fillRect(disx,disy+6,3,17,BLACK); //Actualiza el desplazamiento del disparo
        disy-= 3; //Modifica la altura del disparo
    }
      else{ //reinicia el proyectil en y = 129 luego de que se sale de la pantalla
        GUI(); //Actualiza la GUI para que el disparo no la atraviese
        disy = posy - 19;
        disdraw = false;

    }
  }
}

alienT1::alienT1(){
    posx = posix;
    tdesp = 0;
    tsprite = 0;
    velocidad = 10;
    ancho = 22;
    largo = 16;
    draw = true;
    impact = false;
}

void alienT1::desplazar(){
    if(draw == true){
      if(millis() > tdesp + 400){   //Tiempo de desplzamiento
        tdesp = millis();
        if(posx > posix +10){
          velocidad = velocidad * (-1);
          }
        else if(posx < posix){
          velocidad = velocidad *(-1);
          }
        posx -= velocidad;
      }
      if(millis() - tsprite <= 200){ //Intervalo de tiempo para el primer fotograma
          tft.drawBitmap(posx,posy,alien10,ancho,largo,GREEN);
          tft.drawBitmap(posx,posy,alien10inv,ancho,largo,BLACK);
          tft.fillRect(posx+ancho,posy,abs(velocidad),largo,BLACK);
          tft.fillRect(posx-abs(velocidad),posy,abs(velocidad),largo,BLACK);
        }
        else if(millis() - tsprite <= 400){ //Intervalo de tiempo para el segundo fotograma
          tft.drawBitmap(posx,posy,alien11,ancho,largo,GREEN);
          tft.drawBitmap(posx,posy,alien11inv,ancho,largo,BLACK);
          tft.fillRect(posx+ancho,posy,abs(velocidad),largo,BLACK);
          tft.fillRect(posx-abs(velocidad),posy,abs(velocidad),largo,BLACK);
          }
        else{
          tsprite = millis(); //Reinicia el tiempo
          }
      }
}

void alienT1::impacto(){
    tft.fillRect(posx,posy,ancho,largo,BLACK);
    tft.drawBitmap(posx,posy,boom,22,16,YELLOW);
    delay(200);
    tft.fillRect(10,posy,160,largo,BLACK);
}

void player::init(){
    Serial.begin(9600);
    pinMode(16, INPUT_PULLUP); //   |
    pinMode(2, INPUT_PULLUP); //    |
    pinMode(4, INPUT_PULLUP); //    |Pines de los botones
    pinMode(17, INPUT_PULLUP);//    |
    randomSeed(analogRead(A7)); //Semilla para generar numeros aleatorios
    tft.initR(INITR_BLACKTAB); // Inicializa el ST7735
    tft.fillScreen(ST7735_BLACK); //Rellena la pantalla con un color (ST7735_color);
    tft.setRotation(1); //Rotacion de la pantalla. 0 y 2 para modo vertical, 1 y 3 para modo horizontal.
    tft.setTextWrap(false); //(?)
    tft.setCursor(10,20); //Desplazar el cursor
    tft.setTextColor(ST7735_WHITE); //Color del texto
    tft.setTextSize(2); //
    tft.println("Iniciando...");
    delay(1000);
    tft.fillScreen(ST7735_BLACK); //Color de fondo
}

void player::GUI(){
    tft.fillRect(0,0,160,10,BLACK); //Borra la GUI anterior para dibujar la nueva
    tft.drawLine(0,10,160,10,WHITE); //Dibuja la linea que separa
    tft.setCursor(5,0); //Mueve el cursor
    tft.setTextSize(1); //Tamaño de texto
    tft.print("Puntos: "); //Despliega el texto puntos
    tft.print(puntos); //Imprime el valor de puntos
    if(vidas <= 3){
        for (int i = 0;i <=(vidas-1);i++) {
            tft.drawBitmap(100+i*(15), 0, hearth,11,8,WHITE);
        }
    }
    else {
        tft.drawBitmap(100, 0, hearth,11,8,WHITE);
        tft.setCursor(115,0); //Mueve el cursor
        tft.print("x ");
        tft.print(vidas);
    }
}

alien_h::alien_h(){
    tdesp = 0;
    tsprite = 0;
    velocidad = 10;
    ancho = 16;
    largo = ancho;
    draw = true;
    impact = false;
    posix = 60;
    posx = posix;
    posy = 30;
    disdraw = false;
    disy = posy + 16;
    velproyectil = 1;
}

void alien_h::desplazar(){
    if(draw == true){
      if(millis() > tdesp + 400){   //Tiempo de desplzamiento
        tdesp = millis();
        if(posx > posix +10){
          velocidad = velocidad * (-1);
          }
        else if(posx < posix){
          velocidad = velocidad *(-1);
          }
        posx -= velocidad;
      }
      if(millis() - tsprite <= 200){ //Intervalo de tiempo para el primer fotograma
          tft.drawBitmap(posx,posy,alien20,ancho,largo,CYAN);
          tft.drawBitmap(posx,posy,alien20inv,ancho,largo,BLACK);
          tft.fillRect(posx+ancho,posy,abs(velocidad),largo,BLACK);
          tft.fillRect(posx-abs(velocidad),posy,abs(velocidad),largo,BLACK);
        }
        else if(millis() - tsprite <= 400){ //Intervalo de tiempo para el segundo fotograma
          tft.drawBitmap(posx,posy,alien21,ancho,largo,CYAN);
          tft.drawBitmap(posx,posy,alien21inv,ancho,largo,BLACK);
          tft.fillRect(posx+ancho,posy,abs(velocidad),largo,BLACK);
          tft.fillRect(posx-abs(velocidad),posy,abs(velocidad),largo,BLACK);
          }
        else{
          tsprite = millis(); //Reinicia el tiempo
          }
      }
}

void alien_h::atacar(){
    if(shot == true && disdraw == false && draw == true){  //Si apreto el boton y no estoy dibujando ningun disparo
      disx = posx + 8; //Posicion en X donde se apreto el boton, mueve la posicion en X al centro del sprite de la nave
      disdraw = true; //Dibujar disparo -> true
      }
    if(disdraw == true){  //Si estoy dibujando disparo
      if(disy < 138){ //Limites de la pantalla
        tft.drawBitmap(disx, disy, bala,3,6,YELLOW); //Dibuja el disparo desde la posicion X donde se apreto el boton  y la posicion Y
        tft.fillRect(disx,disy-6,3,6,BLACK); //Actualiza el desplazamiento del disparo
        disy+= velproyectil; //Modifica la altura del disparo
    }
      else{ //reinicia el proyectil en y = 129 luego de que se sale de la pantalla
        //GUI(); //Actualiza la GUI para que el disparo no la atraviese
        disy = posy + 16;
        disdraw = false;
        shot = false;
    }
  }
}

void alien_h::impacto(){
    tft.fillRect(posx,posy,ancho,largo,BLACK);
    tft.drawBitmap(posx,posy,boom,22,16,YELLOW);
    delay(200);
    tft.fillRect(10,posy,160,largo,BLACK);
}

void alien_h::removeshot(){
    tft.fillRect(disx,disy-5,3,11,BLACK);
}

void player::colide(){
    vidas -=1;
    GUI();
    //tft.fillRect(posx,13,23,128,BLACK);
    tft.drawBitmap(posx, posy, nave, ancho, largo,RED);
    delay(200);
    tft.fillRect(posx,posy,ancho,largo,BLACK);
}

void player::dead(){
    int t = 150;
    int opcion = 0;
    unsigned long ts;
    if(draw == true){
        tft.fillRect(0,0,160,128,BLACK);
        draw = false;
    }
    else {
        tft.setTextSize(2);
        tft.setCursor(20,20);
        tft.print("H");
        delay(t);
        tft.print("A");
        delay(t);
        tft.print("S");
        delay(t);
        tft.print(" M");
        delay(t);
        tft.print("U");
        delay(t);
        tft.print("E");
        delay(t);
        tft.print("R");
        delay(t);
        tft.print("T");
        delay(t);
        tft.print("O");
        delay(t);
        tft.drawBitmap(68,50,skull,22,18,WHITE);
        tft.setCursor(40,90);
        tft.setTextSize(1);
        delay(500);
        tft.println("Jugar de nuevo");
        while (true) {
            if(millis() - ts <= 200){ //Intervalo de tiempo para el primer fotograma
                tft.drawBitmap(30,90,select,5,7,WHITE);
              }
            else if(millis() - ts <= 400){ //Intervalo de tiempo para el segundo fotograma
                tft.fillRect(30,90,5,7,BLACK);
            }
            else{
                ts = millis(); //Reinicia el tiempo
            }
            if(digitalRead(16) == 0){
                opcion = 1;
            }
            if(opcion == 1){
                vidas = 3;
                puntos = 0;
                tft.fillRect(0,0,160,128,BLACK);
                draw = true;
                GUI();
                break;
            }
        }
    }
}

void player::next_lvl(){
    int t = 100;
    tft.fillRect(0,0,160,128,BLACK);
    tft.drawLine(40,40,120,40,WHITE);
    tft.drawLine(40,41,120,41,WHITE);
    tft.drawLine(25,70,135,70,WHITE);
    tft.drawLine(25,71,135,71,WHITE);
    tft.setTextSize(2);
    tft.setCursor(40,50);
    tft.print("N");
    delay(t);
    tft.print("I");
    delay(t);
    tft.print("V");
    delay(t);
    tft.print("E");
    delay(t);
    tft.print("L ");
    delay(t);
    tft.print(nivel);
    delay(1500);
    tft.fillRect(0,0,160,128,BLACK);
    GUI();
}

int player::menu(){
    int t = 90;
    bool flag = false;
    int opcion = 0;
    unsigned long ts;
    int pos = 80;
    tft.fillRect(0,0,160,128,BLACK);
    tft.setCursor(50,10);
    tft.setTextSize(2);
    tft.print("S");
    delay(t);
    tft.print("P");
    delay(t);
    tft.print("A");
    delay(t);
    tft.print("C");
    delay(t);
    tft.print("E");
    delay(t);
    tft.setCursor(30,30);
    tft.print("I");
    delay(t);
    tft.print("N");
    delay(t);
    tft.print("V");
    delay(t);
    tft.print("A");
    delay(t);
    tft.print("D");
    delay(t);
    tft.print("E");
    delay(t);
    tft.print("R");
    delay(t);
    tft.print("S");
    tft.setTextSize(1);
    tft.setCursor(40,50);
    tft.setTextColor(ST7735_GREEN);
    tft.print("UdeA Edition");
    while (true) {
        tft.setCursor(40,80);
        tft.setTextColor(ST7735_WHITE);
        tft.print("Un jugador");
        tft.setCursor(40,95);
        tft.print("Dos jugadores");
        flag = false;
        if(digitalRead(16) == 0){
            opcion = opcion +1;
            tft.fillRect(30,80,5,7,BLACK);
            tft.fillRect(30,95,5,7,BLACK);
        }
        if(opcion > 1){
            opcion = 0;
        }
        if (opcion == 0) {
            pos = 80;
        }
        else if(opcion ==1) {
            pos = 95;
        }
        if(millis() - ts <= 250){ //Intervalo de tiempo para el primer fotograma
            tft.drawBitmap(30,pos,select,5,7,WHITE);
          }
        else if(millis() - ts <= 500){ //Intervalo de tiempo para el segundo fotograma
            tft.fillRect(30,pos,5,7,BLACK);
        }
        else{
            ts = millis(); //Reinicia el tiempo
        }
        if(digitalRead(4) == 0){
            flag = true;
        }
        if(flag == true && opcion == 0){
            return opcion;
            break;
        }
        else if (flag == true && opcion == 1) {
            return opcion;
            break;
        }
        delay(100);
    }
}

void player::mjnext_lvl(){
    int t = 100;
    tft.fillRect(0,0,160,128,BLACK);
    tft.setTextSize(1);
    tft.setCursor(60,30);
    if(tag == 1){
        tft.setTextColor(ST7735_BLUE);
    }
    else {
        tft.setTextColor(ST7735_MAGENTA);
    }
    tft.print("Jugador ");
    tft.print(tag);
    tft.drawLine(40,40,120,40,WHITE);
    tft.drawLine(40,41,120,41,WHITE);
    tft.drawLine(25,70,135,70,WHITE);
    tft.drawLine(25,71,135,71,WHITE);
    tft.setTextSize(2);
    tft.setTextColor(ST7735_WHITE);
    tft.setCursor(40,50);
    tft.print("N");
    delay(t);
    tft.print("I");
    delay(t);
    tft.print("V");
    delay(t);
    tft.print("E");
    delay(t);
    tft.print("L ");
    delay(t);
    tft.print(nivel);
    delay(1500);
    tft.fillRect(0,0,160,128,BLACK);
    GUI();
}

void player::mjdead(){
    int t = 150;
    int opcion = 0;
    int x;
    unsigned long ts;
    tft.fillRect(0,0,160,128,BLACK);
    tft.setTextSize(2);
    tft.setCursor(20,20);
    tft.print("H");
    delay(t);
    tft.print("A");
    delay(t);
    tft.print("S");
    delay(t);
    tft.print(" M");
    delay(t);
    tft.print("U");
    delay(t);
    tft.print("E");
    delay(t);
    tft.print("R");
    delay(t);
    tft.print("T");
    delay(t);
    tft.print("O");
    delay(t);
    tft.drawBitmap(68,50,skull,22,18,WHITE);
    delay(500);
    if(tag ==1){
        tft.setCursor(35,90);
        tft.setTextSize(1);
        x = 25;
        tft.println("Siguiente jugador");
    }
    else{
        tft.setCursor(50,90);
        tft.setTextSize(1);
        x = 40;
        tft.println("Continuar");
    }
    tft.println();
    while(true){
            if(millis() - ts <= 200){ //Intervalo de tiempo para el primer fotograma
                tft.drawBitmap(x,90,select,5,7,WHITE);
              }
            else if(millis() - ts <= 400){ //Intervalo de tiempo para el segundo fotograma
                tft.fillRect(x,90,5,7,BLACK);
            }
            else{
                ts = millis(); //Reinicia el tiempo
            }
            if(digitalRead(16) == 0){
                opcion = 1;
            }
            if(opcion == 1){
                delay(1000);
                break;
            }
    }
}

void player::resumen(int puntos1, int puntos2, int lvls1, int lvls2, int enemies1, int enemies2){
    int t = 150;
    int x = 30;
    int opcion = 0;
    unsigned long ts;
    tft.fillRect(0,0,160,128,BLACK);
    tft.setTextSize(1);
    tft.setCursor(45,10);
    tft.print("Fin del juego");
    tft.setCursor(10,30);
    tft.setTextColor(ST7735_BLUE);
    tft.print("Jugador 1");
    tft.setCursor(10,45);
    tft.setTextColor(ST7735_WHITE);
    tft.print("Puntos: ");
    tft.print(puntos1);
    tft.setCursor(12,60);
    tft.print("Nivel");
    tft.setCursor(0,70);
    tft.print("alcanzado: ");
    tft.print(lvls1);
    tft.setCursor(6,85);
    tft.print("Enemigos");
    tft.setCursor(0,95);
    tft.print("derrotados: ");
    tft.setCursor(20,105);
    tft.print(enemies1);
    tft.setCursor(90,30);
    tft.setTextColor(ST7735_MAGENTA);
    tft.print("Jugador 2");
    tft.setTextColor(ST7735_WHITE);
    tft.setCursor(90,45);
    tft.print("Puntos: ");
    tft.print(puntos2);
    tft.setCursor(92,60);
    tft.print("Nivel");
    tft.setCursor(80,70);
    tft.print("alcanzado: ");
    tft.print(lvls2);
    tft.setCursor(86,85);
    tft.print("Enemigos");
    tft.setCursor(80,95);
    tft.print("derrotados: ");
    tft.setCursor(100,105);
    tft.print(enemies2);
    tft.setCursor(40,115);
    tft.print("Jugar de nuevo");
    while(true){
            if(millis() - ts <= 200){ //Intervalo de tiempo para el primer fotograma
                tft.drawBitmap(x,115,select,5,7,WHITE);
              }
            else if(millis() - ts <= 400){ //Intervalo de tiempo para el segundo fotograma
                tft.fillRect(x,115,5,7,BLACK);
            }
            else{
                ts = millis(); //Reinicia el tiempo
            }
            if(digitalRead(16) == 0){
                opcion = 1;
            }
            if(opcion == 1){
                delay(1000);
                break;
            }
    }
}

boss_a::boss_a(){
    vida = 5;
    tdesp = 0;
    tsprite = 0;
    velocidad = 15;
    ancho = 30;
    largo = 33;
    draw = true;
    impact = false;
    posix = 60;
    posx = posix;
    posy = 30;
    disdraw = false;
    disy = posy + 16;
    velproyectil = 1;
}

void boss_a::desplazar()
{
    if(draw == true){
      healthbar();
      if(millis() > tdesp + 400){   //Tiempo de desplzamiento
        tdesp = millis();
        if(posx > posix +10){
          velocidad = velocidad * (-1);
          }
        else if(posx < posix){
          velocidad = velocidad *(-1);
          }
        posx -= velocidad;
      }
      if(millis() - tsprite <= 200){ //Intervalo de tiempo para el primer fotograma
          tft.drawBitmap(posx,posy,bossa0,ancho,largo,MAGENTA);
          tft.drawBitmap(posx,posy,bossa0inv,ancho,largo,BLACK);
          tft.fillRect(posx+ancho,posy,abs(velocidad),largo,BLACK);
          tft.fillRect(posx-abs(velocidad),posy,abs(velocidad),largo,BLACK);
        }
        else if(millis() - tsprite <= 400){ //Intervalo de tiempo para el segundo fotograma
          tft.drawBitmap(posx,posy,bossa1,ancho,largo,MAGENTA);
          tft.drawBitmap(posx,posy,bossa1inv,ancho,largo,BLACK);
          tft.fillRect(posx+ancho,posy,abs(velocidad),largo,BLACK);
          tft.fillRect(posx-abs(velocidad),posy,abs(velocidad),largo,BLACK);
          }
        else{
          tsprite = millis(); //Reinicia el tiempo
          }
      }
}

void boss_a::healthbar(){
    tft.fillRect(0,11,160,10,BLACK); //Borrar la anterior barra de vida
    for (int i = 0;i <=(vida-1);i++) {
        tft.fillRect(20+(i*24),14,19,5,YELLOW);
    }
}

void boss_a::impacto(){
    tft.fillRect(0,posy,160,33,BLACK);
    tft.drawBitmap(posx,posy,bossa0,ancho,largo,RED);
    vida -=1;
    healthbar();
    delay(200);
    tft.fillRect(0,posy,160,33,BLACK);
}
