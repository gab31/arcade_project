#include <Player.h>
#include <Adafruit_GFX.h>
#include <Adafruit_ST7735.h>

#define BLACK    0x0000
#define BLUE     0x001F
#define RED      0xF800
#define GREEN    0x07E0
#define CYAN     0x07FF
#define MAGENTA  0xF81F
#define YELLOW   0xFFE0
#define WHITE    0xFFFF


int totalaliens = 0; //Numero total de aliens en pantalla
int numaliens = 4; //numero de aliens por fila
int randomnumber;
int game_mode;
int activeplayer = 1;
unsigned long t_ataque = 0; //Medida del tiempo para que un alien dispare
int ta = 1700; //Intervalo de tiempo entre el cual los aliens disparan, disminuye cuando la dificultad aumenta


player player1(1,BLUE); //Constructor del jugador 1
player player2(2,MAGENTA); //Constructor del jugador 2
alienT1 alien0[4]; //Array de aliens pacificos (verdes)
alien_h alien1[4]; //Array de aliens hostiles (azules)
boss_a jefe; //Constructor del jefe

void setup() {
  player1.init(); //Inicializacion de la pantalla
  game_mode = player1.menu(); //Menu principal
}

void loop() {
  if(game_mode == 0){ //Modo para un solo jugador
    if(player1.vidas > 0){ //Si el jugador tiene vidas y hay mas de un alien en pantalla se ejecuta la mecanica principal del juego
      if(totalaliens > 0){ 
        if(player1.nivel == 3){ //Nivel del boss
          mech_boss();
          }
        else{
          main_mech(&player1); //Mecanica principal del juego
        }
      }
      else{ //Si no hay aliens en pantalla quiere decir que el jugador superó el nivel
        next_lvl(&player1); //Se reinicia el nivel
        player1.color = WHITE; //Color de la nave en blanco
      }
    }
      else{ //Si llega a cero las vidas indica que el jugador ha muerto
          player1.dead(); //Transicion de muerte del jugador
          player1.nivel = 0; //Reinicia el nivel del jugador a 0
          totalaliens = 0; //Se lleva el numero de aliens al valor por defecto que es 0
          gen_aliens(numaliens); //Genera de nuevo los aliens pacificos
          gen_aliens1(numaliens); //Genera de nuevo los aliens hostiles
        }
    }
    if(game_mode == 1){ //Modo para dos jugadores
      if(activeplayer == 1){
        if(player1.vidas > 0){ //Si el jugador tiene vidas y hay mas de un alien en pantalla se ejecuta la mecanica principal del juego
        if(totalaliens > 0){ 
          main_mech(&player1); //Mecanica principal del juego
          }
        else{ //Si no hay aliens en pantalla quiere decir que el jugador superó el nivel
          mjnext_lvl(&player1); //Se reinicia el nivel
        }
      }
      else{ //Si llega a cero las vidas indica que el jugador1 ha muerto
          player1.mjdead(); //Transicion de muerte del jugador 1
          totalaliens = 0; //Se lleva el numero de aliens al valor por defecto que es 0
          activeplayer = 2;
        }
      }
      else if(activeplayer == 2){
        if(player2.vidas > 0){ //Si el jugador tiene vidas y hay mas de un alien en pantalla se ejecuta la mecanica principal del juego
        if(totalaliens > 0){ 
          main_mech(&player2); //Mecanica principal del juego
          }
        else{ //Si no hay aliens en pantalla quiere decir que el jugador superó el nivel
          mjnext_lvl(&player2); //Se reinicia el nivel
          totalaliens = 8; //Se reinicia el contador de aliens
        }
      }
      else{ //Si llega a cero las vidas indica que el jugador1 ha muerto
          player2.mjdead(); //Transicion de muerte del jugador 1
          totalaliens = 0; //Se lleva el numero de aliens al valor por defecto que es 0
          activeplayer = 0;
        }
      }
      else if(activeplayer ==0){
        resumen(); //Resumen de la partida por turnos
        player1.vidas = 3; //Se reinician las estadisticas de ambos jugadores
        player2.vidas = 3;
        player1.nivel = 0;
        player2.nivel = 0;
        player1.puntos = 0;
        player2.puntos = 0;
        player1.enemies = 0;
        player2.enemies = 0;
        totalaliens = 0;
        activeplayer = 1;
      }
    }
}

void gen_aliens(int _numaliens){ //Genera los aliens verdes
  for(int i = 0; i <= (_numaliens-1);i++){
     alien0[i].impact = false; //Reinicia los atributos de los aliens a los valores por defecto del constructor
     alien0[i].draw = true;
     alien0[i].velocidad = 10;
     alien0[i].posix = 0;
     alien0[i].posx = 0;
     alien0[i].tsprite = 0;
     alien0[i].tdesp = 0;
     alien0[i].posix = 10 + i*(alien0[i].ancho+2*alien0[i].velocidad); //Desplaza las puntos para el dibujado de los alines
     alien0[i].posx = alien0[i].posix; //Posicion en X igual a la posicion inicial
     alien0[i].posy = 60; // //Establece la altura del alien     
    }
 }
 void colision(int _numaliens,player *jugador){ //Comprueba las colisiones
  for(int i = 0; i <= (_numaliens-1);i++){
    if(alien0[i].impact == false){ //Si el alien no ha tenido impactos realiza la comprobacion de la hitbox
      if((jugador->disx >= alien0[i].posx && jugador->disx <= (alien0[i].posx + alien0[i].ancho))&&(jugador->disy >= alien0[i].posy && jugador->disy <= (alien0[i].posy + alien0[i].largo))){ //Condicion de hitbox del alien
        jugador->impacto(); //Rutina del proyectil en caso de impacto
        jugador->puntos += 10; //Aumenta los puntos
        jugador->GUI(); //Actualiza la interfaz grafica
        jugador->enemies += 1; //Aumenta el contador de enemigos derrotados
        alien0[i].impact = true; //Le dice al alien que hubo un impacto
        alien0[i].tsprite = 0; //Reinicia el tiempo de las animaciones
        alien0[i].draw = false; //Deja de dibujar el alien impactado
        alien0[i].impacto(); //Se corre la rutina impacto del alien
        totalaliens-=1; //Disminuye el numero de aliens en pantalla
      }
    }
  }
}
 void colision1(int _numaliens,player *jugador){ //Comprueba las colisiones
  for(int i = 0; i <= (_numaliens-1);i++){
    if(alien1[i].impact == false){ //Si el alien no ha tenido impactos realiza la comprobacion de la hitbox
      if((jugador->disx >= alien1[i].posx && jugador->disx <= (alien1[i].posx + alien1[i].ancho))&&(jugador->disy >= alien1[i].posy && jugador->disy <= (alien1[i].posy + alien1[i].largo))){ //Condicion de hitbox del alien
        jugador->impacto(); //Rutina del proyectil en caso de impacto
        jugador->puntos += 20; //Aumenta los puntos
        jugador->enemies += 1; //Aumenta el contador de enemigos derrotados
        totalaliens -=1; //Disminuye el numero total de aliens
        jugador->GUI(); //Actualiza la interfaz grafica
        alien1[i].tsprite = 0; //Reinicia el tiempo de sprites
        alien1[i].draw = false; //Deja de dibujar el alien impactado
        alien1[i].impacto(); //Se corre la rutina impacto del alien
        alien1[i].impact = true; //Le dice al alien que hubo un impacto para que no vuelva a iterar la colision sobre el mismo objetivo
      }
    }
  }
}

void colide_boss(player *jugador){ //Colisiones con el jefe
      if(jefe.draw == true){ //Si el jefe esta vivo se comprueba la colision
      if((jugador->disx >= jefe.posx && jugador->disx <= (jefe.posx + jefe.ancho))&&(jugador->disy >= jefe.posy && jugador->disy <= (jefe.posy + jefe.largo))){ //Condicion de hitbox del alien
        jugador->impacto(); //Rutina del proyectil en caso de impacto
        jugador->GUI(); //Actualiza la interfaz grafica
        jefe.impacto(); //Se corre la rutina impacto del jefe
        if(jefe.vida == 0){ //Si el jefe muere el numero total de aliens se reduce
            totalaliens -=1;
          }
      }
    }
    
  }

void col_nave(int _numaliens,player *jugador){ //Compreba si algun proyectil impacta al jugador
    for(int i = 0; i <= (_numaliens-1);i++){ //Recorre todos los proyectiles en pantalla
      //Condicion de hitbox del jugador
      if((alien1[i].disx >= jugador->posx && alien1[i].disx <= (jugador->posx + jugador->ancho))&&(alien1[i].disy >= jugador->posy && alien1[i].disy <= (jugador->posy + jugador->largo))){ 
        alien1[i].disdraw = false; //Indica que el alien en cuestion no esta disparando
        alien1[i].removeshot(); //Remueve el disparo de la pantalla
        alien1[i].disy = alien1[i].posy + 16; //Reinicia la posicion del disparo del alien
        jugador->colide(); //Funcion del jugador cuando lo golpea un proyectil
      }
    }
  }

void main_mech(player *jugador){ //Mecanica y logica principal del juego
    jugador->desplazar(); //Desplaza al jugador
    jugador->disparar(); //Comprueba si el jugador apreto el boton de disparo y desplaza el proyectil
    for(int i = 0; i <= (numaliens-1); i++){
      alien0[i].desplazar(); //Desplazamiento y dibujado del alien
    }
    for(int i = 0; i <= (numaliens-1); i++){
      alien1[i].desplazar(); //Desplazamiento y dibujado del alien
    }
    colision(numaliens,jugador); //Comprueba si hay impactos sobre los aliens verdes
    colision1(numaliens,jugador); //Comprueba si hay impactos sobre los aliens azules
    if(millis() > t_ataque + ta){   //Tiempo entre ataques de los aliens
      t_ataque = millis(); //Reinicia el tiempo de espera entre ataques
      randomnumber = random(4); //Escoge un numero aleatorio entre 0 y 4
      alien1[randomnumber].shot = true; //Dispara el alien en la posicion del numero aleatorio
    } 
    for(int i = 0; i <= (numaliens-1); i++){
      alien1[i].atacar(); //Ataques de los aliens azules
    }
    col_nave(numaliens,jugador); //Comprueba si algun proyectil en pantalla impacta la nave del jugador
  }

void mech_boss(){ //Logica del nivel del jefe
    player1.desplazar(); //Desplaza al jugador
    player1.disparar(); //Comprueba si el jugador apreto el boton de disparo y desplaza el proyectil
    jefe.desplazar(); //Desplaza al jefe
    colide_boss(&player1); //Comprueba si hay colisiones sobre el jefe
  }

void next_lvl(player *jugador){ //Funcion que se ejecuta al completar un nivel
  if(jugador->nivel == 2){
      totalaliens = 1;
  }
  else{
    totalaliens = 8; //Se reinicia el contador de aliens
    gen_aliens(numaliens); //Se generan los aliens pacificos
    gen_aliens1(numaliens); //Se generan los aliens hostiles
    ta -=100; //Aumenta la dificultad, los aliens disparan mas rapido;
  }
  jugador->nivel +=1; //Se aumenta el nivel del jugador en 1
  jugador->next_lvl(); //Transicion para dar inicio al nuevo nivel
}
void mjnext_lvl(player *jugador){ //Funcion que se ejecuta al completar un nivel multijugador
  totalaliens = 8; //Se reinicia el contador de aliens
  gen_aliens(numaliens); //Se generan los aliens pacificos
  gen_aliens1(numaliens); //Se generan los aliens hostiles
  ta -=100; //Aumenta la dificultad, los aliens disparan mas rapido
  jugador->nivel +=1; //Se aumenta el nivel del jugador en 1
  jugador->mjnext_lvl(); //Transicion para dar inicio al nuevo nivel
}

void gen_aliens1(int _numaliens){ //Genera los aliens azules 
  for(int i = 0; i <= (_numaliens-1);i++){ //
    //Lleva a cada uno de los aliens a los valores por defecto del constructor
     alien1[i].impact = false; 
     alien1[i].draw = true;
     alien1[i].velocidad = 10;
     alien1[i].posix = 0;
     alien1[i].posx = 0;
     alien1[i].tsprite = 0;
     alien1[i].tdesp = 0;
     alien1[i].posix = 10 + i*(alien1[i].ancho+2*alien1[i].velocidad); //Desplaza las puntos para el dibujado de los alines
     alien1[i].posx = alien1[i].posix; //Posicion en X igual a la posicion inicial
     alien1[i].posy = 30; // //Establece la altura del alien     
    }
 }

 void resumen(){ //Resumen de la partida por turnos
  int puntos[2] = {player1.puntos,player2.puntos}; //Puntos de ambos jugadores
  int lvls[2] = {player1.nivel,player2.nivel}; //Nivel de cada jugador
  int enemigos[2] = {player1.enemies,player2.enemies}; //Enemigos derrotados por cada jugador
  player1.resumen(puntos[0],puntos[1],lvls[0],lvls[1],enemigos[0],enemigos[1]); //Menu resumen al acabar la partida multijugador
  
  }
